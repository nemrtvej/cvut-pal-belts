package pal.Parser;

import pal.Entity.Belt;
import pal.Entity.Disc;
import pal.Entity.InputContainer;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by Marek on 25. 11. 2014.
 */
public class Parser {

    public InputContainer parseInput(BufferedReader bufferedReader) throws IOException {
        String line;

        line = bufferedReader.readLine();
        String[] parameters = line.split(" ");

        int numberOfBelts = Integer.parseInt(parameters[0]);
        int numberOfDiscSegments = Integer.parseInt(parameters[1]);
        int numberOfDiscsOnBelt = Integer.parseInt(parameters[2]);

        InputContainer input = new InputContainer(numberOfBelts, numberOfDiscSegments, numberOfDiscsOnBelt);

        Belt belt;
        for (int i = 0; i<numberOfBelts; i++) {
            belt = this.parseBeltFromInput(bufferedReader, numberOfDiscsOnBelt);
            input.insertBelt(belt);
        }

        return input;
    }

    private Belt parseBeltFromInput(BufferedReader bufferedReader, int numberOfDiscs) throws IOException {
        String line = bufferedReader.readLine();
        String[] parts = line.split(" ");

        int label = Integer.parseInt(parts[0]);
        Belt belt = new Belt(label, numberOfDiscs);

        for (int i = 1; i <= numberOfDiscs; i++) {
            Disc disc = new Disc(parts[i]);
            belt.insertDisc(disc);
        }

        return belt;
    }

}
