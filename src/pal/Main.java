package pal;

import pal.Entity.InputContainer;
import pal.Parser.Parser;
import pal.Rotator.Rotator;
import pal.Solver.Solver;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws Exception {
        InputStreamReader inputStream = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStream);

        Parser parser = new Parser();

        InputContainer inputContainer = parser.parseInput(bufferedReader);

        Solver solver = new Solver();

        Rotator rotator = new Rotator();
        rotator.rotateInputBelts(inputContainer);

        System.out.print(solver.solve(inputContainer).getSolutionString());
    }
}
