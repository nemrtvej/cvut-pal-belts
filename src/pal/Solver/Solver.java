package pal.Solver;

import pal.Entity.Belt;
import pal.Entity.InputContainer;
import pal.Entity.Solution;

public class Solver {
    public Solution solve(InputContainer inputContainer) {
        Solution solution = new Solution();

        for (Belt belt: inputContainer.getBelts()) {
            solution.insertBelt(belt);
        }

        solution.reorder();

        return solution;
    }
}
