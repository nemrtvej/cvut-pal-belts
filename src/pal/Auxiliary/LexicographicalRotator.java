package pal.Auxiliary;

public class LexicographicalRotator {

    /**
     * Returns lexicographically minimal version of given string.
     * Searches lexicographically minimal version even in opposite order.
     * It internally uses http://en.wikipedia.org/wiki/Lexicographically_minimal_string_rotation
     *
     * @param input
     * @return
     */
    public String getLexicographicalMinimum(String input) {
        String reversedInput = new StringBuilder(input).reverse().toString();

        String leastNormalOrder = this.rotateStringToLowestLexicographicalOrder(input);
        String leastReversedOrder = this.rotateStringToLowestLexicographicalOrder(reversedInput);

        if (leastNormalOrder.compareTo(leastReversedOrder) < 0) {
            return leastNormalOrder;
        } else {
            return leastReversedOrder;
        }
    }

    private String rotateStringToLowestLexicographicalOrder(String input) {
        int inputLength = input.length();
        input += input;

        int leastRotationSoFar = 0;
        int[] failureArray = this.createFailureArray(inputLength * 2);

        for (int j = 1; j<2*inputLength; j++) {
            int i = failureArray[j-leastRotationSoFar-1];

            while (i != -1 && input.charAt(j) != input.charAt(leastRotationSoFar+i+1)) {
                if (input.charAt(j) < input.charAt(leastRotationSoFar+i+1)) {
                    leastRotationSoFar = j-i-1;
                }
                i = failureArray[i];
            }

            if (i == -1 && input.charAt(j) != input.charAt(leastRotationSoFar+i+1)) {
                if (input.charAt(j) < input.charAt(leastRotationSoFar+i+1)) {
                    leastRotationSoFar = j;
                }
                failureArray[j-leastRotationSoFar] = -1;
            } else {
                failureArray[j-leastRotationSoFar] = i+1;
            }
        }

        return input.substring(leastRotationSoFar, leastRotationSoFar+inputLength);
    }

    private int[] createFailureArray(int length) {
        int[] result = new int[length];

        for (int i = 0; i<result.length; i++) {
            result[i] = -1;
        }

        return result;
    }

    private void dumpArray(int[] array) {
        for(int value: array) {
            System.out.print(value + " ");
        }
        System.out.println();
    }
}
