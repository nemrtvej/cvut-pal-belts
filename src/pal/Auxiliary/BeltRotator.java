package pal.Auxiliary;

import pal.Entity.Belt;

public class BeltRotator {

    public Belt rotateBelt(Belt belt) {
        Belt reversedBelt = belt.reverse();

        int beltRotationSteps = this.compute(belt);
        int reversedBeltRotationSteps = this.compute(reversedBelt);

        belt.setOffset(beltRotationSteps);
        reversedBelt.setOffset(reversedBeltRotationSteps);

        if (belt.compareTo(reversedBelt) < 0) {
            return belt;
        } else {
            return reversedBelt;
        }
    }

    private int compute(Belt belt) {
        int inputLength = belt.getNumberOfDiscs();

        int leastRotationSoFar = 0;
        int[] failureArray = this.createFailureArray(inputLength * 2);

        for (int j = 1; j<2*inputLength; j++) {

            int i = failureArray[j-leastRotationSoFar-1];

            while (i != -1 && !belt.getDisc(j).equals(belt.getDisc(leastRotationSoFar+i+1))) {
                if (belt.getDisc(j).compareTo(belt.getDisc(leastRotationSoFar+i+1)) < 0) {
                    leastRotationSoFar = j-i-1;
                }
                i = failureArray[i];
            }

            if (i == -1 && !belt.getDisc(j).equals(belt.getDisc(leastRotationSoFar+i+1))) {
                if (belt.getDisc(j).compareTo(belt.getDisc(leastRotationSoFar+i+1)) < 0) {
                    leastRotationSoFar = j;
                }
                failureArray[j-leastRotationSoFar] = -1;
            } else {
                failureArray[j-leastRotationSoFar] = i+1;
            }

        }

        return leastRotationSoFar;
    }

    private int[] createFailureArray(int length) {
        int[] result = new int[length];

        for (int i = 0; i<result.length; i++) {
            result[i] = -1;
        }

        return result;
    }


    private void dumpArray(int[] array) {
        for(int value: array) {
            System.out.print(value + " ");
        }
        System.out.println();
    }
}
