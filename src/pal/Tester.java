package pal;

import pal.Entity.InputContainer;
import pal.Entity.Solution;
import pal.Rotator.Rotator;
import pal.Solver.Solver;

import java.io.IOException;

public class Tester {
    public static void main(String[] args) throws IOException{
        TestSuite testSuite = new TestSuite("c:/Phoenix/CVUT/PAL/Homeworks/4/testCases/");

        TestSuite.TestCase[] testCases = testSuite.getTestCases();

        int i = 1;

        for (pal.TestSuite.TestCase testCase: testCases) {
            try {

                System.out.println("Test case #"+i);
                Solver solver = new Solver();

                long startTime = System.currentTimeMillis();

                InputContainer inputContainer = testCase.getInput();

                Rotator rotator = new Rotator();
                rotator.rotateInputBelts(inputContainer);

                Solution solution = solver.solve(inputContainer);

                long endTime = System.currentTimeMillis() - startTime;

                System.out.println("Expected result: \n"+testCase.getSolutionString());
                System.out.println("Computed result: \n"+solution.getSolutionString());
                System.out.println("Computation time: "+ (endTime/1000.0) + "s");
                System.out.println("Result: " + (solution.getSolutionString().trim().compareTo(testCase.getSolutionString().trim()) == 0 ? "OK" : "FAIL"));

                /*
                    if (solution.getSolutionString().compareTo(testCase.getSolutionString()) != 0) {
                        System.out.println(inputContainer.toString());
                    }
                */

                System.out.println("--------------------------------------------------------------------");

            } catch (NullPointerException e) {
                System.out.println(e.getMessage());
            } finally {
                i++;
            }

        }
    }
}
