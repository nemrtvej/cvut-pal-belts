package pal.Entity;

public class InputContainer {

    private Belt[] belts;

    private int numberOfDiscSegments;

    private int numberOfDiscsOnBelt;

    private int beltPointer;

    public InputContainer(int numberOfBelts, int numberOfDiscSegments, int numberOfDiscsOnBelt) {
        this.belts = new Belt[numberOfBelts];
        this.numberOfDiscSegments = numberOfDiscSegments;
        this.numberOfDiscsOnBelt = numberOfDiscsOnBelt;
        this.beltPointer = 0;
    }

    public void insertBelt(Belt belt) {
        this.belts[beltPointer++] = belt;
    }

    public Belt[] getBelts() {
        return belts;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Belt belt: this.belts) {
            stringBuilder.append(belt.toString());
            stringBuilder.append(belt.getBeltHash());
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }
}
