package pal.Entity;

public class Belt {
    private Disc[] discs;

    private int discPointer;

    private int label;

    private int offset;

    private DiscHashInterface discHashInterface;

    interface DiscHashInterface {
        public String getBeltHash();
    }

    class CompiledHash implements DiscHashInterface {
        private String compiledHash;

        CompiledHash(String compiledHash) {
            this.compiledHash = compiledHash;
        }

        public String getBeltHash() {
            return compiledHash;
        }

    }

    class UncompiledHash implements DiscHashInterface {

        private Belt belt;

        UncompiledHash(Belt belt) {
            this.belt = belt;
        }

        @Override
        public String getBeltHash() {
            String hash = this.computeHash();
            this.belt.discHashInterface = new CompiledHash(hash);

            return hash;
        }

        private String computeHash() {

            StringBuilder stringBuilder = new StringBuilder();
            int length = this.belt.getNumberOfDiscs();
            for (int i = 0; i<length; i++) {
                stringBuilder.append(this.belt.getDisc(i).getLabel());
            }

            return stringBuilder.toString();
        }
    }

    public Belt(int label, int numberOfDiscs) {
        this.discs = new Disc[numberOfDiscs];
        this.discPointer = 0;
        this.label = label;
        this.offset = 0;
        this.discHashInterface = new UncompiledHash(this);
    }

    public Belt(int label, Disc[] discs) {
        this.discs = discs;
        this.discPointer = discs.length;
        this.label = label;
        this.offset = 0;
        this.discHashInterface = new UncompiledHash(this);
    }

    public void insertDisc(Disc disc) {
        this.discHashInterface = new UncompiledHash(this);
        this.discs[discPointer++] = disc;
    }

    public int getLabel() {
        return label;
    }

    public Disc[] getDiscs() {
        return discs;
    }

    public int getNumberOfDiscs() {
        return this.discPointer;
    }

    public Belt reverse() {
        Disc[] discs = new Disc[this.discs.length];
        int arrayPointer = 0;
        for (int i = this.discs.length -1; i>=0; i--) {
            discs[arrayPointer++] = this.getDisc(i);
        }

        return new Belt(this.label, discs);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.label);

        for (int i = 0; i<this.discs.length; i++) {
            stringBuilder.append(" ");
            stringBuilder.append(this.getDisc(i).toString());
        }

        return stringBuilder.toString();
    }

    public Disc getDisc(int index) {
        return this.discs[(this.offset + index) % this.getNumberOfDiscs()];
    }

    public void setOffset(int offset) {
        this.discHashInterface = new UncompiledHash(this);
        this.offset = offset;
    }

    public int compareTo(Belt reversedBelt) {
        return this.discHashInterface.getBeltHash().compareTo(reversedBelt.discHashInterface.getBeltHash());
    }

    public String getBeltHash() {
        return this.discHashInterface.getBeltHash();
    }
}
