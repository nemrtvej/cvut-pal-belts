package pal.Entity;

public class Disc {
    private String label;

    public Disc(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String newLabel) {
        this.label = newLabel;
    }

    @Override
    public String toString() {
        return this.label;
    }

    public boolean equals(Disc otherDisc) {
        return this.label.equals(otherDisc.label);
    }

    public int compareTo(Disc otherDisc) {
        return this.getLabel().compareTo(otherDisc.getLabel());
    }
}
