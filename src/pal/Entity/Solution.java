package pal.Entity;

import java.util.*;

/**
 * Created by Marek on 25. 11. 2014.
 */
public class Solution {

    private List<LinkedList<Belt>> beltSolutions;

    class BeltSolutionComparator implements Comparator<LinkedList<Belt>> {

        @Override
        public int compare(LinkedList<Belt> first, LinkedList<Belt> second) {
            if (first.size() > second.size()) {
                return -1;
            } else if (first.size() < second.size()) {
                return 1;
            } else {
                return Integer.compare(first.getFirst().getLabel(), second.getFirst().getLabel());
            }
        }
    }

    public Solution() {
        this.beltSolutions = new ArrayList<LinkedList<Belt>>();
    }

    public String getSolutionString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(this.beltSolutions.size());

        for (LinkedList<Belt> list: this.beltSolutions) {
            stringBuilder.append("\n");
            stringBuilder.append(this.getStringOfLabels(list));
        }

        return stringBuilder.toString();
    }

    public void insertBelt(Belt belt) {
        for (LinkedList<Belt> list: this.beltSolutions) {
            if (list.getFirst().compareTo(belt) == 0) {
                list.add(belt);
                return;
            }
        }

        LinkedList<Belt> newList = new LinkedList<Belt>();
        newList.add(belt);
        this.beltSolutions.add(newList);
    }

    public void reorder() {
        Collections.sort(this.beltSolutions, new BeltSolutionComparator());
    }

    private String getStringOfLabels(LinkedList<Belt> list) {
        StringBuilder stringBuilder = new StringBuilder();
        Belt[] belts = list.toArray(new Belt[0]);
        for (int i = 0; i<belts.length; i++) {
            stringBuilder.append(belts[i].getLabel());
            if (i+1 != belts.length) {
                stringBuilder.append(" ");
            }
        }

        return stringBuilder.toString();
    }
}
