package pal;

import pal.Entity.InputContainer;
import pal.Parser.Parser;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TestSuite {

    private String directoryPath;

    class TestCase {
        InputContainer input;

        String solutionString;

        TestCase(InputContainer input, String solutionString) {
            this.input = input;
            this.solutionString = solutionString;
        }

        public InputContainer getInput() {
            return input;
        }

        public String getSolutionString() {
            return solutionString;
        }
    }

    public TestSuite(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    TestCase[] getTestCases() throws IOException{

        TestCase[] testCases =  new TestCase[15];

        for (int i = 1; i<16; i++) {
            String number = i>9 ? Integer.toString(i) : "0" + Integer.toString(i);
            testCases[i-1] = this.getTestCase("pub"+number);
        }

        return testCases;
    }

    private TestCase getTestCase(String fileName) throws IOException {
        File inputFile = new File(this.directoryPath + "/" + fileName + ".in");
        File resultFile = new File(this.directoryPath + "/" + fileName + ".out");

        Parser parser = new Parser();

        BufferedReader bufferedInputReader = new BufferedReader(new FileReader(inputFile));
        InputContainer input = parser.parseInput(bufferedInputReader);

        BufferedReader bufferedResultReader = new BufferedReader(new FileReader(resultFile));
        String resultString = new String(Files.readAllBytes(Paths.get(resultFile.getAbsolutePath())));

        return new TestCase(input, resultString);
    }
}
