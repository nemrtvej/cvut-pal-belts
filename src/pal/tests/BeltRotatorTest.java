package pal.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pal.Auxiliary.BeltRotator;
import pal.Entity.Belt;
import pal.Entity.Disc;

public class BeltRotatorTest {

    private BeltRotator beltRotator;

    @BeforeSuite
    public void setUp() throws Exception {
        this.beltRotator = new BeltRotator();
    }

    @Test(dataProvider = "beltRotatorProvider")
    public void testBeltRotator(Belt belt, String expected) throws Exception {
        Belt result = this.beltRotator.rotateBelt(belt);

        String resultString = this.createResultString(result);

        Assert.assertEquals(resultString, expected);
    }

    private String createResultString(Belt belt) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i<belt.getNumberOfDiscs(); i++) {
            stringBuilder.append(belt.getDisc(i).getLabel());
        }

        return stringBuilder.toString();
    }

    @DataProvider(name = "beltRotatorProvider")
    public static Object[][] provideBeltRotatorData() {
        return new Object[][] {
                {new Belt(0, new Disc[]{new Disc("b"), new Disc("b"), new Disc("a"), new Disc("a"), new Disc("a"), }), "aaabb"},
                {new Belt(0, new Disc[]{new Disc("c"), new Disc("b"), new Disc("a"), new Disc("a"), new Disc("a"), }), "aaabc"},
                {new Belt(0, new Disc[]{new Disc("a"), new Disc("b"), new Disc("a"), new Disc("a"), new Disc("a"), }), "aaaab"},
                {new Belt(0, new Disc[]{new Disc("a"), new Disc("a"), new Disc("c"), new Disc("d"), new Disc("a"), }), "aaacd"},
                {new Belt(0, new Disc[]{new Disc("a"), new Disc("a"), new Disc("a"), new Disc("a"), new Disc("a"), }), "aaaaa"},
                {new Belt(0, new Disc[]{new Disc("a"), new Disc("d"), new Disc("c"), new Disc("b"), }), "abcd"},
        };
    }
}
