package pal.tests;

import pal.Auxiliary.LexicographicalRotator;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class LexicographicalRotatorTest {

    private LexicographicalRotator lexicographicalRotator;

    @BeforeSuite
    public void setUp() throws Exception {
        this.lexicographicalRotator = new LexicographicalRotator();
    }

    @Test(dataProvider = "lexicographicalStringsProvider")
    public void testGetLexicographicalMinimum(String string, String expected) throws Exception {
        String result = this.lexicographicalRotator.getLexicographicalMinimum(string);
        Assert.assertEquals(result, expected);
    }

    @DataProvider(name = "lexicographicalStringsProvider")
    public static Object[][] provideLexicographicalTestData() {
        return new Object[][] {
                {"aba", "aab"},
                {"aacda", "aaacd"},
                {"ba", "ab"},
                {"ab", "ab"},
                {"cba", "abc"},
                {"aabaa", "aaaab"},
                {"adcb", "abcd"},
        };
    }
}