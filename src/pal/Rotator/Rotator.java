package pal.Rotator;

import pal.Auxiliary.BeltRotator;
import pal.Auxiliary.LexicographicalRotator;
import pal.Entity.Belt;
import pal.Entity.Disc;
import pal.Entity.InputContainer;

public class Rotator {
    BeltRotator beltRotator;

    LexicographicalRotator lexicographicalRotator;

    public Rotator() {
        this.beltRotator = new BeltRotator();
        this.lexicographicalRotator = new LexicographicalRotator();
    }

    public void rotateInputBelts(InputContainer inputContainer) {

        Belt[] belts = inputContainer.getBelts();

        for (int i = 0; i<belts.length; i++) {
            belts[i] = this.rotateBelt(belts[i]);
        }
    }

    private Belt rotateBelt(Belt belt) {
        for (Disc disc: belt.getDiscs()) {
            this.rotateDisc(disc);
        }

        return this.beltRotator.rotateBelt(belt);
    }

    private void rotateDisc(Disc disc) {
        disc.setLabel(this.lexicographicalRotator.getLexicographicalMinimum(disc.getLabel()));
    }
}
